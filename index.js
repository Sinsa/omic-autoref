const Config = require('./config.json');
let logChannel;
const bancho = require('bancho.js');
const schedule = require('node-schedule');
const express = require('express');
const {google} = require('googleapis');

// region load additional stuff

const qualifiers = require('./autoref/qualifiers');

// endregion

const banchoClient = new bancho.BanchoClient({
    username: Config.osuUsername,
    password: Config.osuIrcPassword,
    apiKey: Config.osuApiToken
});

const app = express();
app.use(express.json());

app.listen(15139, async () => {
    try {
        await banchoClient.connect();
        console.log(
            `${new Date().toISOString()}\tConnected to osu IRC via the user ${
                Config.osuUsername
            }!`
        );
    } catch (e) {
        console.error(
            `${new Date().toISOString()}\tCould not connect to osu IRC! Reason: ${
                e.message
            }`
        );
    }
    console.log("<init> I am ready!");
    schedule.scheduleJob('0 45 * * * *', callSheetCheck);
    await callSheetCheck();
});

app.get('/kill', async (_request, response) => {
    console.log("Forced shutdown via HTTP Request...");
    await response.status(200).send("Shutdown triggered...");
    process.exit(0);
});

app.get('/check', async (_request, response) => {
    const now = new Date();
    if (now.getMinutes() > 29) {
        await response.status(400).send("Cannot initiate a check now because it would interfere with the cronJob at minute :45!");
    } else {
        console.log("Forced check via HTTP initiated");
        await response.status(200).send("Check forced...");
        await callSheetCheck();
    }
});

async function callSheetCheck() {
    console.log("Performing sheet check...");
    const sheets = await authorize();
    const response = await sheets.spreadsheets.values.get({
        spreadsheetId: Config.schedulesheetId,
        range: 'Sheet1!A2:L20',
    });
    const lobbies = await convertSheetDataQualifiers(response.data.values);
    for (const lobby of lobbies) {
        if (lobby.datetime && lobby.datetime > Date.now() && lobby.datetime < Date.now() + 1800000) {
            if (lobby.players.length > 0) {
                if (!lobby.referee || lobby.referee === "" || lobby.referee.toLowerCase() === "autoref") {
                    const createMatchTime = new Date(lobby.datetime) - 300000;
                    if (Date.now() > createMatchTime) {
                        await qualifiers.refMatch(lobby, {banchoClient});
                    } else {
                        schedule.scheduleJob(createMatchTime, function () {
                            qualifiers.refMatch(lobby, {banchoClient});
                        });
                    }
                }
            } else {
                console.log(`Lobby ${lobby.id} wasn't created and autoreffed because no one signed up for it.`);
            }
        }
    }

}

async function authorize() {
    const authentication = new google.auth.GoogleAuth({
        keyFile: 'credentials.json',
        scopes: 'https://www.googleapis.com/auth/spreadsheets.readonly'
    });

    const sheetsClient = await authentication.getClient();

    return google.sheets({
        version: 'v4',
        auth: sheetsClient
    });
}


async function convertSheetDataQualifiers(results) {
    let sheet = [];
    results.forEach(result => {
        sheet.push({
            stage: 'qualifiers',
            id: result[0],
            date: result[1],
            time: result[2],
            datetime: getDate(result[1], result[2]) + 7200000,
            referee: result[3],
            players: result.slice(4, 11).filter(player => player) // get result[4] to result[11] and remove all empty strings
        });
    });
    return sheet;
}

// This function checks the style of date (if its written 2022/05/07 or 07-05-2022) and parses it into a JS Date Object.
// Throws exception when the Date format is not known because the whole refsheet reminder cannot check in that case.
// It also makes sure the time is e.g. 09:30 and not 9:30.
function getDate(date, time) {
    if (time.match('^\\d{1}:\\d{2}$')) {
        time = '0' + time;
    }
    let parsedDate;
    if (date.includes('/')) {
        const internaldate = date.split('/');
        parsedDate = Date.parse(
            `${internaldate[2]}-${internaldate[1]}-${internaldate[0]}T${time}`
        );
    } else if (date.includes('-')) {
        parsedDate = Date.parse(`${date}T${time}`);
    } else {
        throw new DOMException(
            'The Date format supplied by the sheet is not supported.',
            'DateFormatNotSupportedException'
        );
    }
    if (typeof parsedDate !== 'number') {
        throw new Error(`DateParseError \t parsing timestring "${time}" returned ${parsedDate} (typeof ${typeof parsedDate})!`);
    } else {
        return parsedDate;
    }
}