const Config = require('../config.json');
const osu = require('node-osu');
const {google} = require("googleapis");
module.exports = {
    async refMatch(match, params) {
        console.log(`Creating Qualifiers lobby for ID ${match.id}...`);
        const lobbyIrcChannel = await params.banchoClient.createLobby(`OMIC 2022: (Qualifiers) vs (Lobby ${match.id})`);
        lobbyIrcChannel.setMaxListeners(20);
        const lobby = lobbyIrcChannel.lobby;
        const password = generatePassword(5);
        await lobby.setPassword(password);
        console.log(`Password for Lobby ${match.id}: ${password}`);
        const sheets = await authorize();
        await sleep(5000);
        try {
            await sheets.spreadsheets.values.clear({
                spreadsheetId: Config.refsheetId,
                range: `${match.id}!C4:F4`
            });
        } catch {
            console.log(`Couldnt clear mp link field ${match.id}!C4:F4 because it was empty.`)
        }
        await sheets.spreadsheets.values.append({
            spreadsheetId: Config.refsheetId,
            range: `${match.id}!C4:F4`,
            valueInputOption: 'USER_ENTERED',
            resource: {
                values: [[lobby.getHistoryUrl()]]
            }
        })
        await lobby.setMap(3410699, 3);
        await lobby.setSettings(0, 3, match.players.length);
        await lobby.setMods([], true);
        await lobby.startTimer(600);
        for (const player of match.players) {
            await lobby.invitePlayer(player);
        }
        let playersInLobby = 0;

        lobby.on('playerJoined', (player) => {
            if (!match.players.includes(player.player.user.username)) {
                lobby.kickPlayer(player.player.user.ircUsername);
            } else {
                playersInLobby++;
            }
        });
        lobby.on('playerLeft', () => {
            playersInLobby--;
        });
        while (playersInLobby < match.players.length && match.datetime + 300000 > Date.now()) {
            await sleep(1000);
        }
        //If the time is up and there's only noshows, we'll close the lobby.
        if (playersInLobby === 0) {
            await lobby.closeLobby();
            await lobbyIrcChannel.removeAllListeners();
            return;
        }
        await lobby.abortTimer();
        await main(match, lobby, lobbyIrcChannel, params);
    }
};

function generatePassword(length) {
    let result = '';
    const characters = 'abcdefghijkmnpqrstuvwxyz123456789';
    const charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(
            Math.floor(Math.random() * charactersLength)
        );
    }
    return result;
}

async function main(match, lobby, lobbyIrcChannel, params) {
    let abortAllowed = true;
    let playthroughIndex = 0;
    const beatmaps = [
        {
            designation: "Stage 1",
            beatmapId: 3676731
        },
        {
            designation: "Stage 2",
            beatmapId: 3676726
        },
        {
            designation: "Stage 3",
            beatmapId: 3677199
        },
        {
            designation: "Stage 4",
            beatmapId: 3676738
        },
        {
            designation: "Stage 5",
            beatmapId: 3676730
        }
    ]

    await lobbyIrcChannel.sendMessage(`Welcome to the qualifiers lobby ${match.id} of OMIC!`);
    await sleep(5000);
    await lobbyIrcChannel.sendMessage('We\'re gonna play each map twice. Between the two playthroughs, there\'s gonna be a 5 minute break.');
    await sleep(5000);
    await lobbyIrcChannel.sendMessage('Incase you\'re encountering technical problems, type abort to abort the map. This only works within the first 30 seconds of the map, and only once per lobby.');
    await sleep(5000);
    await lobbyIrcChannel.sendMessage('And thats about it! If you have any questions, send them into the #general channel in the discord!');
    await sleep(5000);

    //Setting some variables and listeners here so no unnecessary listeners are created every time
    let readyToPlay;
    let matchEnded;
    let matchAborted;
    let matchStartTime;

    lobby.on('allPlayersReady', () => {
        lobby.updateSettings();
        readyToPlay = true;
    });

    lobbyIrcChannel.on('message', (message) => {
        if (message.user.ircUsername.toLowerCase() === 'banchobot' && message.message === 'Countdown finished') {
            readyToPlay = true;
        }
    });

    lobby.on('matchFinished', () => {
        matchEnded = true;
    });
    lobby.on('matchAborted', () => {
        matchAborted = true;
    });

    while (playthroughIndex < 2) {
        let beatmapIndex = 0;
        while (beatmapIndex < beatmaps.length) {
            await lobby.setMap(beatmaps[beatmapIndex].beatmapId, 3);
            const osuApi = new osu.Api(Config.osuApiToken, {});
            const currentBeatmap = await osuApi.getBeatmaps({b: beatmaps[beatmapIndex].beatmapId});
            await lobbyIrcChannel.sendMessage(`Next Map: ${beatmaps[beatmapIndex].designation} | ${currentBeatmap[0].artist} - ${currentBeatmap[0].title} [${currentBeatmap[0].version}] mapped by ${currentBeatmap[0].creator}`);
            await lobbyIrcChannel.sendMessage('Please ready up!');
            await lobby.startTimer(120);
            readyToPlay = false;

            while (!readyToPlay) {
                await sleep(1000);
            }
            await lobby.startMatch(10);
            await lobbyIrcChannel.sendMessage('Good luck and have fun!');
            matchStartTime = Date.now();
            matchEnded = false;
            matchAborted = false;

            lobbyIrcChannel.on('message', async (message) => {
                if (message.message === 'abort') {
                    if (abortAllowed) {
                        if (Date.now() < matchStartTime + 60000) {
                            abortAllowed = false;
                            await lobbyIrcChannel.sendMessage(`The map was aborted by ${message.user.ircUsername}.`);
                            await lobby.abortMatch();
                        } else {
                            await lobbyIrcChannel.sendMessage('The map can only be aborted within the first 30 seconds of the map!');
                        }
                    } else {
                        await lobbyIrcChannel.sendMessage('A map can only be aborted once per lobby!');
                    }
                }
            });

            while (!matchEnded && !matchAborted) {
                await sleep(1000);
            }
            if (matchAborted) {
                await lobbyIrcChannel.sendMessage('Map was aborted. Restarting map...');
            } else {
                beatmapIndex++;
            }


        }
        playthroughIndex++;
        if (playthroughIndex === 1) {
            await lobbyIrcChannel.sendMessage('That\'s it for the first playthrough! We\'ll continue after a 5 minute break.');
            await lobby.startTimer(300);

            let isBreakOver = false;
            lobbyIrcChannel.on('message', (message) => {
                if (message.user.ircUsername.toLowerCase() === 'banchobot' && message.message === 'Countdown finished') {
                    isBreakOver = true;
                }
            });
            while (!isBreakOver) {
                await sleep(1000);
            }
        } else {
            await lobbyIrcChannel.sendMessage('And that would be it! Thanks for playing, you all did great. You\'re now free to leave. Good luck for qualifying!');
            await lobbyIrcChannel.sendMessage('This lobby will automatically close in one minute.');
        }
    }
    await console.log(`🎮 | **Lobby done**\t| Qualifiers Lobby ${match.id} was successfully autoreffed. Match history: <${lobby.getHistoryUrl()}>`);
    await sleep(60000);
    await lobby.closeLobby();
    lobby.removeAllListeners();
    lobbyIrcChannel.removeAllListeners();
}

async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


async function authorize() {
    const authentication = new google.auth.GoogleAuth({
        keyFile: 'credentials.json',
        scopes: 'https://www.googleapis.com/auth/spreadsheets'
    });

    const sheetsClient = await authentication.getClient();

    return google.sheets({
        version: 'v4',
        auth: sheetsClient
    });
}